docker pull rabbitmq
docker network create rabbits
# docker run -d --rm --net rabbits --hostname rabbit-1 --name rabbit-1 rabbitmq:3.11-management -e RABBITMQ_ERLANG_COOKIE=FSHEVCXBBETJJVJWTOWT  -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management
docker run -it --rm --net rabbits --name rabbit-1 --hostname rabbit-1 -e RABBITMQ_ERLANG_COOKIE=FSHEVCXBBETJJVJWTOW -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management
# RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS="-setcookie FSHEVCXBBETJJVJWTOWT"

# how to grab existing erlang cookie
# docker exec -it rabbit-1 cat /var/lib/rabbitmq/.erlang.cookie
# docker run -d --rm --net rabbits -p 8080:15672 -e RABBITMQ_ERLANG_COOKIE=FSHEVCXBBETJJVJWTOWT --hostname rabbit-manager --name rabbit-manager rabbitmq:3.8-management

#join the manager

# docker exec -it rabbit-manager rabbitmqctl stop_app
# docker exec -it rabbit-manager rabbitmqctl reset
# docker exec -it rabbit-manager rabbitmqctl join_cluster rabbit@rabbit-1
# docker exec -it rabbit-manager rabbitmqctl start_app
# docker exec -it rabbit-manager rabbitmqctl cluster_status

docker exec -it rabbit-1 rabbitmqctl cluster_status

#join node 2

docker exec -it rabbit-2 rabbitmqctl stop_app
docker exec -it rabbit-2 rabbitmqctl reset
docker exec -it rabbit-2 rabbitmqctl join_cluster rabbit@rabbit-1
docker exec -it rabbit-2 rabbitmqctl start_app
docker exec -it rabbit-2 rabbitmqctl cluster_status

#join node 3
docker exec -it rabbit-3 rabbitmqctl stop_app
docker exec -it rabbit-3 rabbitmqctl reset
docker exec -it rabbit-3 rabbitmqctl join_cluster rabbit@rabbit-1
docker exec -it rabbit-3 rabbitmqctl start_app
docker exec -it rabbit-3 rabbitmqctl cluster_status


# docker run -d --rm --net rabbits `-v ${Rogue87}/config/rabbit-1/:/config/ `-e RABBITMQ_CONFIG_FILE=/config/rabbitmq `-e RABBITMQ_ERLANG_COOKIE=WIWVHCDTCIUAWANLMQAW `--hostname rabbit-1 `--name rabbit-1 `-p 8081:15672 `rabbitmq:3.8-management