﻿namespace RabbitConsumer.Services
{
    public class MessageHandler
    {
        public static bool HandleMessage(String message)
        {
            try
            {
                Console.WriteLine("Handler processing message:" + message);
                //JsonConverter<Object>
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error processing message:" + ex);
                return false;

            }

        }
    }
}
