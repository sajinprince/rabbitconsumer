using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.HttpSys;
using RabbitConsumer.Services;
using RabbitMQ.Client;
using System.ComponentModel;

namespace RabbitConsumer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController : ControllerBase
    {
        private readonly ILogger<HealthCheckController> _logger;
        private IMessageWorker _worker; 

        public HealthCheckController(ILogger<HealthCheckController> logger,IMessageWorker worker )
        {
            _logger = logger;
            _worker = worker;
        }

        [HttpGet(Name = "HealthCheck")]
        public ActionResult  Get()
        {
            try
            {
                var connection=_worker.SetupConnection();
                connection.Dispose();
                return Ok("Rabbit Connection Sucessfull");
            }
            catch (Exception ex)
            {
                //return BadRequest(ex.Message);
                return StatusCode(500,ex.Message);
                //return BadRequest(ex.Message);
            }
            
                  
           
        }
        //create an http endpoint that publishes a rabbit message       
        [HttpPost(Name = "PublishMessage")]
        public ActionResult Post([FromBody] string message)
        {
            try
            {
                //for perforamnce move SetupConnection to the controller constructor but only needed under heavy load
                _worker.Publish((message) => { Console.WriteLine("sending message"); }, _worker.SetupConnection()) ;
                return Ok("Message Published");
            }
            catch (Exception ex)
            {
                //return BadRequest(ex.Message);
                return StatusCode(500,ex.Message);
            }
             
           
        }   

    }
}