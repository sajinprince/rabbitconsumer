﻿using RabbitMQ.Client;

namespace RabbitConsumer.Services
{
    public interface IMessageWorker
    {
        public void ConnectandConsume(Func<string, bool> messageHandler, string quename, string exchangename, string key);
        public IConnection SetupConnection();

        public void Publish( Action<string> jsonMessage, IConnection connection);    
    }
}
