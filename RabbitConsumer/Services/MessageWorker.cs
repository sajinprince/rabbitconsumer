﻿using Microsoft.AspNetCore.Connections;
using System.Timers;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace RabbitConsumer.Services
{
    public class MessageWorker : IMessageWorker
    {  
            private IConfigurationSection _config;

        public MessageWorker(IConfiguration config)
            {
                _config = config.GetSection("ConsumerConfig");
                ConnectandConsume(MessageHandler.HandleMessage, _config.GetValue<string>("QName")??"noQfromconfig", _config.GetValue<string>("ExchangeName")??"NoExchange", _config.GetValue<string>("RoutingKey")??"#");
                //ConnectandConsumeTest(HandleMessage); //just testing the worker pattern please remove
            }

            public IConnection SetupConnection()
            {
                var factory = new ConnectionFactory { HostName = _config.GetValue<string>("Hostname") ?? "no-hostname-fromconfig",Port=5672 };
                return factory.CreateConnection();
            }

            //public void ConnectandConsume(System.EventHandler<RabbitMQ.Client.Events.BasicDeliverEventArgs> eventHandler, Func<string,bool> messageHandler)
            public void ConnectandConsume(Func<string, bool> messageHandler, string quename, string exchangename, string key)
            {
                var connection = SetupConnection();

                var channel = connection.CreateModel();
                channel.ExchangeDeclare(exchange: exchangename, type: "topic");
                channel.QueueDeclare(queue: quename,
                                    durable: false,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);
                channel.QueueBind(queue: quename,
                exchange: exchangename,
                routingKey: key);

                Console.WriteLine(" [*] Waiting for messages.");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    var success = messageHandler(message);
                    if (success == false)
                    {
                        Console.WriteLine("Handler failed processing message:" + message);
                    }
                };

                //consumer.Received += messageHandler;
                channel.BasicConsume(queue: quename,
                                        autoAck: true,
                                        consumer: consumer);
            }

            private void ConnectandConsumeTest(Func<string, bool> messageHandler)
            {
                var aTimer = new System.Timers.Timer(2000);
                aTimer.Elapsed += OnTimedEvent;
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
            }
            private void OnTimedEvent(Object source, ElapsedEventArgs e)
            {
                MessageHandler.HandleMessage("TestMessage");
            }
       
            public void Publish(Action<string> jsonMessage,IConnection connection)
            {
                var channel = connection.CreateModel();
                channel.ExchangeDeclare(exchange: _config.GetValue<string>("ExchangeName") ?? "NoExchange", type: "topic");

                var message = jsonMessage.ToString();
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: _config.GetValue<string>("ExchangeName")??"NoExchange",
                routingKey: _config.GetValue<string>("RoutingKey")??"#",
                basicProperties: null,body: body);
                Console.WriteLine(" [x] Sent {0}", message);
                connection.Close(); 
            }
            

                
        
    }
}
